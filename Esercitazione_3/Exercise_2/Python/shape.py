class Point:
    def __init__(self, x: float, y: float):
        pass


class IPolygon:
    def area(self) -> float:
        return 0.0


class Ellipse(IPolygon):
    def __init__(self, center: Point, a: int, b: int):
        pass


class Circle(IPolygon):
    def __init__(self, center: Point, radius: int):
        pass


class Triangle(IPolygon):
    def __init__(self, p1: Point, p2: Point, p3: Point):
        pass


class TriangleEquilateral(IPolygon):
    def __init__(self, p1: Point, edge: int):
        pass


class Quadrilateral(IPolygon):
    def __init__(self, p1: Point, p2: Point, p3: Point, p4: Point):
        pass


class Parallelogram(IPolygon):
    def __init__(self, p1: Point, p2: Point, p4: Point):
        pass


class Rectangle(IPolygon):
    def __init__(self, p1: Point, base: int, height: int):
        pass


class Square(IPolygon):
    def __init__(self, p1: Point, edge: int):
        pass
