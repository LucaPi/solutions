#ifndef SHAPE_H
#define SHAPE_H

#define SQUARE(X) X*X

#include <iostream>
#include <math.h>

using namespace std;

namespace ShapeLibrary {

  class Point {
    public:
      double X;
      double Y;
      Point(const double& x=0,
            const double& y=0);
      Point(const Point& point);
      double Length(Point& p2);
  };

  class IPolygon {
    public:
      virtual double Area() const = 0;
  };

  class Ellipse : public IPolygon
  {
    private:
      Point Center;
      int A;
      int B;
    public:
      Ellipse(const Point& center,
              const int& a,
              const int& b);

      double Area() const { return M_PI*A*B; }
  };

  class Circle : public IPolygon
  {
    private:
      Point Center;
      int Radius;
    public:
      Circle(const Point& center,
             const int& radius);

      double Area() const { return Ellipse(Center, Radius, Radius).Area(); }
  };


  class Triangle : public IPolygon
  {
    private:
      Point P1;
      Point P2;
      Point P3;
    public:
      Triangle(const Point& p1,
               const Point& p2,
               const Point& p3);

      double Area() const {return fabs(P1.X*P2.Y+P2.X*P3.Y+P3.X*P1.Y-P2.X*P1.Y-P3.X*P2.Y-P1.X*P3.Y)/2;}
  };


  class TriangleEquilateral : public IPolygon
  {
      Point P1;
      int EDGE;
    public:
      TriangleEquilateral(const Point& p1,
                          const int& edge);

      double Area() const { return (sqrt(3)*SQUARE(EDGE))/4; }
  };

  class Quadrilateral : public IPolygon
  {
      Point P1;
      Point P2;
      Point P3;
      Point P4;
    public:
      Quadrilateral(const Point& p1,
                    const Point& p2,
                    const Point& p3,
                    const Point& p4);

      double Area() const {return(fabs(P1.X*P2.Y+P2.X*P3.Y+P3.X*P4.Y+P4.X*P1.Y-P2.X*P1.Y-P3.X*P2.Y-P4.X*P3.Y-P1.X*P4.Y)/2);}
  };


  class Parallelogram : public IPolygon
  {
      Point P1;
      Point P2;
      Point P4;
    public:
      Parallelogram(const Point& p1,
                    const Point& p2,
                    const Point& p4);

      double Area() const { return Quadrilateral(P1,P2,Point(P2.X+P4.X-P1.X,P4.Y+P2.Y-P1.Y),P4).Area(); }
  };

  class Rectangle : public IPolygon
  {
      Point P1;
      int Base;
      int Height;
    public:
      Rectangle(const Point& p1,
                const int& base,
                const int& height);

      double Area() const { return Base*Height; }
  };

  class Square: public IPolygon
  {
      Point P1;
      int EDGE;
    public:
      Square(const Point& p1,
             const int& edge);

      double Area() const { return SQUARE(EDGE); }
  };
}

#endif // SHAPE_H
