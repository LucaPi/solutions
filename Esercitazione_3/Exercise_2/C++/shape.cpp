#include "shape.h"
#include <math.h>

namespace ShapeLibrary {

Point::Point(const double &x, const double &y)
{
    X = x;
    Y = y;
}

Point::Point(const Point &point)
{
    X = point.X;
    Y = point.Y;
}

double Point::Length(Point& p2)
{
    return(sqrt((X*X-p2.X*p2.X)+(Y*Y-p2.Y*p2.Y)));
}

Ellipse::Ellipse(const Point &center, const int &a, const int &b)
{
    Center = center;
    A = a;
    B = b;
}

Circle::Circle(const Point &center, const int &radius)
{
    Center = center;
    Radius = radius;
}

Triangle::Triangle(const Point &p1, const Point &p2, const Point &p3)
{
    P1 = p1;
    P2 = p2;
    P3 = p3;
}

TriangleEquilateral::TriangleEquilateral(const Point &p1, const int &edge)
{
    P1 = p1;
    EDGE = edge;
}

Quadrilateral::Quadrilateral(const Point &p1, const Point &p2, const Point &p3, const Point &p4)
{
    P1 = p1;
    P2 = p2;
    P3 = p3;
    P4 = p4;
}

Square::Square(const Point &p1, const int &edge)
{
    P1 = p1;
    EDGE = edge;
}

Rectangle::Rectangle(const Point &p1, const int &base, const int &height)
{
    P1 = p1;
    Base = base;
    Height = height;
}

Parallelogram::Parallelogram(const Point &p1, const Point &p2, const Point &p4)
{
    P1 = p1;
    P2 = p2;
    P4 = p4;
}


}
