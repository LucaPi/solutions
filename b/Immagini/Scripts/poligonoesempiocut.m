close all
clear all
clc
points1 = [1.0,1.5;
6.0,2.0;
6.3,4.48;
5.9,4.5;
5.4,4.5;
5.0,4.0;
4.1,4.6;
2.81628,4.65814;
2.5,4.5;
2.3,4.7;
2.0,4.7;
2.0,4.7;
1.4,4.7];
points2 = [6.3,4.5;
6.5,6.0;
5.4,4.5;
5.9,4.5];
points3 = [4.1,4.6;
3.5,5.0;
2.81628,4.65814];
points4 = [2.3,4.7;
1.5,5.5;
1.4,4.7;
2.0,4.7;
2.0,4.7];

polygon1 = polyshape(points1(:, 1),points1(:, 2));
polygon2 = polyshape(points2(:, 1),points2(:, 2));
polygon3 = polyshape(points3(:, 1),points3(:, 2));
polygon4 = polyshape(points4(:, 1),points4(:, 2));
segment = [2.0,4.7;
    5.9,4.5]; 

figure;
plot(polygon1);
hold on;
plot(polygon2);
hold on;
plot(polygon3);
hold on;
plot(polygon4)
hold on;
plot(segment(:, 1), segment(:, 2), 'r--');
hold on;
plot(points1(:, 1),points1(:, 2), 'ko');
hold on;
plot(points2(:, 1),points2(:, 2), 'ko');
hold on;
plot(points3(:, 1),points3(:, 2), 'ko');
hold on;
plot(points4(:, 1),points4(:, 2), 'ko');
hold on;
plot(segment(:, 1), segment(:, 2), 'r*');