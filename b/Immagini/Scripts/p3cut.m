close all
clear all
clc

points1 = [1.5,1;
5.6,1.5;
5.5,4.8;
4.20433,6.00929;
4.1,5.9;
3.72131,5.50328;
3.2,4.2;
2.4086,4.12805;
2,3.7;
1.19122,2.8527];
points2 = [4,6.2;
3.72131,5.50328;
4.1,5.9;
4.20433,6.00929]; 
points3 = [1,4;
1.19122,2.8527;
2,3.7;
2.4086,4.12805]; 


polygon1 = polyshape(points1(:, 1),points1(:, 2));
polygon2 = polyshape(points2(:, 1),points2(:, 2));
polygon3 = polyshape(points3(:, 1),points3(:, 2));
segment = [2.0,3.7;
    4.1,5.9]; 

figure;
plot(polygon1);
hold on;
plot(polygon2);
hold on;
plot(polygon3);
hold on;
plot(segment(:, 1), segment(:, 2), 'r--');
hold on;
plot(points1(:, 1),points1(:, 2), 'ko');
hold on;
plot(points2(:, 1),points2(:, 2), 'ko');
hold on;
plot(points3(:, 1),points3(:, 2), 'ko');
hold on;
plot(segment(:, 1), segment(:, 2), 'r*');