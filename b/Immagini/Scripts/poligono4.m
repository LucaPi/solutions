points = [2.0,-1.0;
                7.0,1.0;
                7.0,2.0;
                5.0,3.7;
                2.0,5.0;
                1.0,3.0];
polygon = polyshape(points(:, 1),points(:, 2));
segment = [2.0,3.4;
        5.6,1.3]; 

figure;
plot(polygon);
hold on;
plot(segment(:, 1), segment(:, 2), 'r--');
hold on;
plot(points(:, 1),points(:, 2), 'ko');
hold on;
plot(segment(:, 1), segment(:, 2), 'r*');

% txt = '0';
% text(2.0,-1.0,txt,'HorizontalAlignment','Right','VerticalAlignment','Top')
% txt = '1';
% text(7.0,1.0,txt,'HorizontalAlignment','Left','VerticalAlignment','Top')
% txt = '2';
% text(7.0,2.0,txt,'HorizontalAlignment','Left','VerticalAlignment','Bottom')
% txt = '3';
% text(5-0,3.7,txt,'HorizontalAlignment','Right','VerticalAlignment','Bottom')
% txt = '4';
% text(2.0,5.0,txt,'HorizontalAlignment','Right','VerticalAlignment','Bottom')
% txt = '5';
% text(1.0,3.0,txt,'HorizontalAlignment','Right','VerticalAlignment','Bottom')
% 
% txt = 's1';
% text(2.0,3.4,txt,'Color','r','HorizontalAlignment','Right','VerticalAlignment','Top')
% txt = 's2';
% text(5.6,1.3,txt,'Color','r','HorizontalAlignment','Right','VerticalAlignment','Top')