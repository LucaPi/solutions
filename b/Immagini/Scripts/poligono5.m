points = [3.0, 1.0;
    4.0, -1.0;
    5.0, 0.5;
    7.0, -0.3;
    6.0, 4.0;
    1.0, 3.0];
polygon = polyshape(points(:, 1),points(:, 2));
segment = [4.0, -0.2;
    6.5, 0.5]; 

figure;
plot(polygon);
hold on;
plot(segment(:, 1), segment(:, 2), 'r--');
hold on;
plot(points(:, 1),points(:, 2), 'ko');
hold on;
plot(segment(:, 1), segment(:, 2), 'r*');


% txt = '0';
% text(3.0,1.0,txt,'HorizontalAlignment','Right','VerticalAlignment','Top')
% txt = '1';
% text(4.0,-1.0,txt,'HorizontalAlignment','Right','VerticalAlignment','Top')
% txt = '2';
% text(5.0,0.5,txt,'HorizontalAlignment','Left','VerticalAlignment','Top')
% txt = '3';
% text(7.0,-0.3,txt,'HorizontalAlignment','Right','VerticalAlignment','Top')
% txt = '4';
% text(6.0,4.0,txt,'HorizontalAlignment','Right','VerticalAlignment','Bottom')
% txt = '5';
% text(1.0,3.0,txt,'HorizontalAlignment','Right','VerticalAlignment','Bottom')
% 
% txt = 's1';
% text(4.0,-0.2,txt,'Color','r','HorizontalAlignment','Right','VerticalAlignment','Top')
% txt = 's2';
% text(6.5,0.5,txt,'Color','r','HorizontalAlignment','Right','VerticalAlignment','Top')