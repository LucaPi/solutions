close all
clear all
clc
points = [1.0,1.5;
    6.0,2.0;
    6.5,6.0;
    5.0,4.0;
    3.5,5.0,
    2.5,4.5;
    1.5,5.5];

polygon = polyshape(points(:, 1),points(:, 2));
segment = [2.0,4.7;
    5.9,4.5]; 

figure;
plot(polygon);
hold on;
plot(segment(:, 1), segment(:, 2), 'r--');
hold on;
plot(points(:, 1),points(:, 2), 'ko');
hold on;
plot(segment(:, 1), segment(:, 2), 'r*');