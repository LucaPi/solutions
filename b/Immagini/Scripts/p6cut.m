close all
clear all
clc

points1 = [2,4;
2,-0.6875;
2.5,-0.5;
3.00909,-0.309091;
3.5,1;
3.97368,0.0526316;
4.29592,0.173469]; 
points2 = [2,-3;
3.00909,-0.309091;
2.5,-0.5;
2,-0.6875]; 
points3 = [5,-2;
6.14706,0.867647;
5.88462,0.769231;
5,-1;
4.29592,0.173469;
3.97368,0.0526316]; 
points4 = [7,3;
5.88462,0.769231;
6.14706,0.867647];

segment = [2.5, -0.5;
    6.5, 1.0];
polygon1 = polyshape(points1(:, 1),points1(:, 2));
polygon2 = polyshape(points2(:, 1),points2(:, 2));
polygon3 = polyshape(points3(:, 1),points3(:, 2));
polygon4 = polyshape(points4(:, 1),points4(:, 2));

figure;
plot(polygon1);
hold on;
plot(polygon2);
hold on;
plot(polygon3);
hold on; 
plot(polygon4);
hold on;
plot(points1(:, 1),points1(:, 2), 'ko');
hold on;
plot(points2(:, 1),points2(:, 2), 'ko');
hold on;
plot(points3(:, 1),points3(:, 2), 'ko');
hold on;
plot(points4(:, 1),points4(:, 2), 'ko');
hold on
plot(segment(:, 1), segment(:, 2), 'r--');
hold on;
plot(segment(:, 1), segment(:, 2), 'r*');