close all
clear all
clc
points = [2.0, -3.0;
    3.5, 1.0;
    5.0, -2.0;
    7.0, 3.0;
    5.0, -1.0;
    2.0, 4.0];
polygon = polyshape(points(:, 1),points(:, 2));
segment = [2.5, -0.5;
    6.5, 1.0];

plot(polygon);
hold on;
plot(segment(:, 1), segment(:, 2), 'r--');
hold on;
plot(segment(:, 1), segment(:, 2), 'r*');