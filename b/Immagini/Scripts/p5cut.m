close all
clear all
clc
points1 = [3,1;
3.64912,-0.298246;
4,-0.2;
4.65574,-0.0163934;
5,0.5;
5.61765,0.252941;
6.5,0.5;
6.79476,0.582533;
6,4;
1,3]; 
points2 = [4,-1;
4.65574,-0.0163934;
4,-0.2;
3.64912,-0.298246];
points3 = [7,-0.3;
6.79476,0.582533;
6.5,0.5;
5.61765,0.252941]; 

polygon1 = polyshape(points1(:, 1),points1(:, 2));
polygon2 = polyshape(points2(:, 1),points2(:, 2));
polygon3 = polyshape(points3(:, 1),points3(:, 2));

figure;
plot(polygon1);
hold on;
plot(polygon2);
hold on;
plot(polygon3);
hold on;
plot(points1(:, 1),points1(:, 2), 'ko');
hold on;
plot(points2(:, 1),points2(:, 2), 'ko');
hold on;
plot(points3(:, 1),points3(:, 2), 'ko');
hold on
plot(segment(:, 1), segment(:, 2), 'r--');
hold on;
plot(segment(:, 1), segment(:, 2), 'r*');