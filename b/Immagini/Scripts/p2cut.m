close all
clear all
clc

points1 = [2.5,1;
4,2.1;
1.4,2.75;
3.6,2.2;
1.2,2.8;
1,2.1];
points2 = [3.4,4.2;
1.6,4.2;
1.2,2.8;
1.4,2.75;
3.6,2.2;
4,2.1];

polygon1 = polyshape(points1(:, 1),points1(:, 2));
polygon2 = polyshape(points2(:,1),points2(:,2));
segment = [1.4,2.75;
    3.6,2.2]; 

figure;
plot(polygon1);
hold on;
plot(polygon2);
hold on;
plot(points1(:, 1),points1(:, 2), 'ko');
hold on;
plot(points2(:, 1),points2(:, 2), 'ko');
hold on;
plot(segment(:, 1), segment(:, 2), 'r--');
hold on;
plot(segment(:, 1), segment(:, 2), 'r*');