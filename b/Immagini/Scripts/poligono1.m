points = [1.0,1.0;
    5.0,1.0;
    5.0,3.1;
    1.0,3.1];
polygon = polyshape(points(:, 1),points(:, 2));
segment = [2.0,1.2;
    4.0,3.0]; 

figure;
plot(polygon);
hold on;
plot(segment(:, 1), segment(:, 2), 'r--');
hold on;
plot(points(:, 1),points(:, 2), 'ko');
hold on;
plot(segment(:, 1), segment(:, 2), 'r*');

% txt = '0';
% text(1.0,1.0,txt,'HorizontalAlignment','Right','VerticalAlignment','Top')
% txt = '1';
% text(5.0,1.0,txt,'HorizontalAlignment','Left','VerticalAlignment','Top')
% txt = '2';
% text(5.0,3.1,txt,'HorizontalAlignment','Left','VerticalAlignment','Bottom')
% txt = '3';
% text(1.0,3.1,txt,'HorizontalAlignment','Right','VerticalAlignment','Bottom')
% 
% txt = 's1';
% text(2.0,1.2,txt,'Color','r','HorizontalAlignment','Right','VerticalAlignment','Top')
% txt = 's2';
% text(4.0,3.0,txt,'Color','r','HorizontalAlignment','Left','VerticalAlignment','Top')