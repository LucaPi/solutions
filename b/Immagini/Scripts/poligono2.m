clear all
close all
clc
points = [2.5,1.0;
    4.0,2.1;
    3.4,4.2;
    1.6,4.2;
    1.0,2.1];
polygon = polyshape(points(:, 1),points(:, 2));
segment = [1.4,2.75;
    3.6,2.2]; 

figure;
plot(polygon);
hold on;
plot(segment(:, 1), segment(:, 2), 'r--');
hold on;
plot(points(:, 1),points(:, 2), 'ko');
hold on;
plot(segment(:, 1), segment(:, 2), 'r*');

% txt = '0';
% text(2.5,1.0,txt,'HorizontalAlignment','Right','VerticalAlignment','Top')
% txt = '1';
% text(4.0,2.1,txt,'HorizontalAlignment','Left','VerticalAlignment','Top')
% txt = '2';
% text(3.4,4.2,txt,'HorizontalAlignment','Left','VerticalAlignment','Bottom')
% txt = '3';
% text(1.6,4.2,txt,'HorizontalAlignment','Right','VerticalAlignment','Bottom')
% txt = '4';
% text(1.0,2.1,txt,'HorizontalAlignment','Right','VerticalAlignment','Bottom')
% 
% txt = 's1';
% text(1.4,2.75,txt,'Color','r','HorizontalAlignment','Right','VerticalAlignment','Top')
% txt = 's2';
% text(3.6,2.2,txt,'Color','r','HorizontalAlignment','Left','VerticalAlignment','Top')