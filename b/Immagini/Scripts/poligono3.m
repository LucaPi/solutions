points = [1.5,1.0;
          5.6,1.5;
          5.5,4.8;
          4.0,6.2;
          3.2,4.2;
          1.0,4.0];

segment = [2.0,3.7;
           4.1,5.9]; 

polygon = polyshape(points(:, 1),points(:, 2));


figure;
plot(polygon);
hold on;
plot(segment(:, 1), segment(:, 2), 'r--');
hold on;
plot(points(:, 1),points(:, 2), 'ko');
hold on;
plot(segment(:, 1), segment(:, 2), 'r*');


% txt = '0';
% text(1.5,1.0,txt,'HorizontalAlignment','Right','VerticalAlignment','Top')
% txt = '1';
% text(5.6,1.5,txt,'HorizontalAlignment','Left','VerticalAlignment','Top')
% txt = '2';
% text(5.5,4.8,txt,'HorizontalAlignment','Left','VerticalAlignment','Bottom')
% txt = '3';
% text(4.2,6.0,txt,'HorizontalAlignment','Right','VerticalAlignment','Bottom')
% txt = '4';
% text(4.1,5.9,txt,'HorizontalAlignment','Right','VerticalAlignment','Bottom')
% txt = '5';
% text(4.0,6.2,txt,'HorizontalAlignment','Right','VerticalAlignment','Bottom')
% txt = '6';
% text(3.7,5.5,txt,'HorizontalAlignment','Right','VerticalAlignment','Bottom')
% txt = '7';
% text(3.2,4.2,txt,'HorizontalAlignment','Right','VerticalAlignment','Bottom')
% txt = '8';
% text(2.4,4.1,txt,'HorizontalAlignment','Right','VerticalAlignment','Bottom')
% txt = '9';
% text(2.0,3.7,txt,'HorizontalAlignment','Right','VerticalAlignment','Bottom')
% txt = '10';
% text(1.0,4.0,txt,'HorizontalAlignment','Right','VerticalAlignment','Bottom')
% txt = '11';
% text(1.2,2.8,txt,'HorizontalAlignment','Right','VerticalAlignment','Bottom')
