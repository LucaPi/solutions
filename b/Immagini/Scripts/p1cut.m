close all 
clear all
clc
points1 = [1,1;
1.77778,1;
2,1.2;
4,3;
4.11111,3.1;
1,3.1]; 
points2 = [5,1;
5,3.1;
4.11111,3.1;
2,1.2;
4,3;
1.77778,1];

polygon1 = polyshape(points1(:, 1),points1(:, 2));
polygon2 = polyshape(points2(:,1),points2(:,2));
segment = [2.0,1.2;
    4.0,3.0]; 

figure;
plot(polygon1);
hold on;
plot(polygon2);
hold on;
plot(segment(:, 1), segment(:, 2), 'r--');
hold on;
plot(points1(:, 1),points1(:, 2), 'ko');
hold on;
plot(points2(:, 1),points2(:, 2), 'ko');
hold on;
plot(segment(:, 1), segment(:, 2), 'r*');