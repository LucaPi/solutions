close all
clear all
clc
points1 = [2,-1;
6.47458,0.789831;
2,3.4;
5.6,1.3;
1.38065,3.76129;
1,3]; 
points2 = [7,1;
7,2;
5,3.7;
2,5;
1.38065,3.76129;
2,3.4;
5.6,1.3;
6.47458,0.789831];  

polygon1 = polyshape(points1(:, 1),points1(:, 2));
polygon2 = polyshape(points2(:, 1),points2(:, 2));
segment = [2.0,3.4;
        5.6,1.3]; 

figure;
plot(polygon1);
hold on;
plot(polygon2);
hold on;
plot(segment(:, 1), segment(:, 2), 'r--');
hold on;
plot(points1(:, 1),points1(:, 2), 'ko');
hold on;
plot(points2(:, 1),points2(:, 2), 'ko');
hold on;
plot(segment(:, 1), segment(:, 2), 'r*');