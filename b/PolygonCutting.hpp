#ifndef POLYGONCUTTING_H
#define POLYGONCUTTING_H

#endif // POLYGONCUTTING_H

#include <vector>
#include <iostream>

using namespace std;

namespace Cut {

enum Type
{
    NoIntersection = 0,
    IntersectionOnLine = 1,
    IntersectionOnSegment = 2,
};

class Point {
public:
    double X;
    double Y;
    bool isInt;

    Point();
    Point(const double& x,
          const double& y);
    Point(const Point& point);

    double ComputeNorm2() const;

    Point operator+(const Point& point) const;
    Point operator-(const Point& point) const;
    Point& operator-=(const Point& point);
    Point& operator+=(const Point& point);
    bool operator==(const Point& point) const;
    bool operator!=(const Point& point) const;
    friend ostream& operator<<(ostream& stream, const Point& point)
    {
        stream << "(" << point.X << "," << point.Y << ")";
        return stream;
    }
};

class Polygon {
public:
    vector<Point> vertices;

    Polygon();
    Polygon(const vector<Point>& points);

    void AddVertex(const Point& vertex);

    bool operator ==(Polygon& polygon);
};


class IUtilities {
public:
    virtual Type CheckIntersection(const Point& s1, const Point& s2, const Point& one, const Point& two) = 0;
    virtual Point IntersectionPoint(const Point &s1, const Point &s2, const Point& one, const Point& two) = 0;
    virtual int FindPoint(const vector<Point>& pointsVector, const Point& pointToFind) = 0;
    virtual bool IsBetween(const Point& l1, const Point& l2, const Point& p) = 0;
    virtual vector<Point> SortPoints(vector<Point>&points) = 0;
    virtual double CalcDistance(const Point& p1, Point& p2) = 0;
    virtual void PrintResults(const vector<Point>& newPoints, const vector<Polygon>& cutPolygons) = 0;
    virtual void PrintResultsMatlab(const vector<Point>& newPoints, const vector<Polygon>& cutPolygons) = 0;
};

class Utilities : public IUtilities
{
private:
    double tolerance = 1.0E-7;
public:
    Utilities() {}

    Type CheckIntersection(const Point& s1, const Point& s2, const Point& one, const Point& two);
    Point IntersectionPoint(const Point &s1, const Point &s2, const Point& one, const Point& two);
    int FindPoint(const vector<Point>& pointsVector, const Point& pointToFind);
    bool IsBetween(const Point& l1, const Point& l2, const Point& p);
    vector<Point> SortPoints(vector<Point>& points);
    double CalcDistance(const Point& p1, Point& p2);
    void PrintResults(const vector<Point>& newPoints, const vector<Polygon>& cutPolygons);
    void PrintResultsMatlab(const vector<Point>& newPoints, const vector<Polygon>& cutPolygons);
};

class PolygonCut {
private:
    vector<Point> newPoints;
    vector<Polygon> cutPolygons;
    IUtilities& computation;
public:
    PolygonCut(Utilities& utilities) : computation(utilities) {}
    PolygonCut();

    void CutPolygon(const vector<Point>& p, const vector<int>& vertices, const vector<Point>& s);
    const vector<Point>& GetNewPoints() { return newPoints; };
    const vector<Polygon>& GetCutPolygons() { return cutPolygons; };
    const vector<vector<int>> GetCutPolygonsIndices();
};


}
