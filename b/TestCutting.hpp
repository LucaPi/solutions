#ifndef TESTCUTTING_H
#define TESTCUTTING_H

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <gmock/gmock-matchers.h>
#include "PolygonCutting.hpp"

using namespace testing;

namespace Cut {
double fRand(double fMin, double fMax)
{
    double f = (double)rand() / RAND_MAX;
    return fMin + f * (fMax - fMin);
}

TEST(TestPoint, TestDefaultConstructorAndCout)
{

    Point point;

    ostringstream streamPoint;
    streamPoint << "(" << point.X << "," << point.Y << ")";
    stringstream buffer;
    streambuf *sbuf = cout.rdbuf();
    cout.rdbuf(buffer.rdbuf());
    cout << point;
    EXPECT_EQ(buffer.str(), streamPoint.str());
    cout.rdbuf(sbuf);
    cout << fflush;
}

TEST(TestPoint, TestCopyConstructor)
{

    Cut::Point center = Cut::Point(fRand(1,2), fRand(2,4));

    Cut::Point center2 = center;

    EXPECT_TRUE(&center != &center2);
    EXPECT_EQ(center.X, center2.X);
    EXPECT_EQ(center.Y, center2.Y);
}
TEST(TestPoint, TestOperator)
{
    Cut::Point center = Cut::Point(fRand(1,2), fRand(2,4));
    Cut::Point center2 = center;

    Cut::Point center3 = center + center2;
    Cut::Point center4 = center - center2;

    EXPECT_TRUE(&center != &center2);
    EXPECT_EQ(center3.X, center2.X+center.X);
    EXPECT_EQ(center3.Y, center2.Y+center.Y);
    EXPECT_EQ(center4.X, center.X-center2.X);
    EXPECT_EQ(center4.Y, center.Y-center2.Y);
    center -= center2;
    EXPECT_TRUE(center.X < 1.0E-16);
    EXPECT_TRUE(center.Y < 1.0E-16);
}
TEST(TestPoint, TestStreamOperator)
{
    Cut::Point center = Cut::Point(fRand(1,2), fRand(2,4));

    EXPECT_TRUE(&center != NULL);
}

TEST(TestPolygonCut, TestCut11)
{
    vector<Point> points{Point(1.0,1.0),
                Point(5.0,1.0),
                Point(5.0,3.1),
                Point(1.0,3.1)};
    vector<int> vertices{0,1,2,3};
    vector<Point> s{Point(2.0,1.2),
                Point(4.0,3.0)};
    Utilities ut;
    PolygonCut cut(ut);
    cut.CutPolygon(points,vertices,s);
    vector<Point> pointsNew{Point(1.0,1.0),
                ut.IntersectionPoint(points[0],points[1],s[0],s[1]),
                Point(5.0,1.0),
                Point(5.0,3.1),
                ut.IntersectionPoint(points[2],points[3],s[0],s[1]),
                Point(1.0,3.1),
                Point(2.0,1.2),
                Point(4.0,3.0)};
    vector<vector<int>> polygonPoints = {
        {0,1,6,7,4,5}, {2,3,4,6,7,1}
    };

    EXPECT_EQ(cut.GetNewPoints(),pointsNew);
    EXPECT_EQ(cut.GetCutPolygonsIndices(), polygonPoints);
}

TEST(TestPolygonCut, TestCut21)
{
    vector<Point> points{
        Point(2.5,1),
                Point(4.0,2.1),
                Point(3.4,4.2),
                Point(1.6,4.2),
                Point(1.0,2.1)
    };
    vector<int> vertices{0,1,2,3,4};
    vector<Point> s{Point(1.4,2.75), Point(3.6,2.2)};
    Utilities ut;
    PolygonCut cut(ut);
    cut.CutPolygon(points,vertices,s);
    vector<Point> pointsNew{
        Point(2.5,1),
                Point(4.0,2.1),
                Point(3.4,4.2),
                Point(1.6,4.2),
                ut.IntersectionPoint(points[3],points[4],s[0],s[1]),
                Point(1.0,2.1),
                Point(1.4,2.75),
                Point(3.6,2.2)
    };
    vector<vector<int>> polygonPoints {
        {0,1,6,7,4,5}, {2,3,4,6,7,1}
    };

    EXPECT_EQ(cut.GetNewPoints(),pointsNew);
    EXPECT_EQ(cut.GetCutPolygonsIndices(),polygonPoints);
}

TEST(TestPolygonCut, TestCut31)
{
    vector<Point> points{
        Point(1.5,1.0),
                Point(5.6,1.5),
                Point(5.5,4.8),
                Point(4.0,6.2),
                Point(3.2,4.2),
                Point(1.0,4.0)
    };
    vector<int> vertices{0,1,2,3,4,5};
    vector<Point> s{
        Point(2.0,3.7),
                Point(4.1,5.9)
    };
    Utilities ut;
    PolygonCut cut(ut);
    cut.CutPolygon(points, vertices, s);
    vector<Point> pointsNew{
        Point(1.5,1.0),
                Point(5.6,1.5),
                Point(5.5,4.8),
                ut.IntersectionPoint(points[2],points[3],s[0],s[1]),
                Point(4.0,6.2),
                ut.IntersectionPoint(points[3],points[4],s[0],s[1]),
                Point(3.2,4.2),
                ut.IntersectionPoint(points[4],points[5],s[0],s[1]),
                Point(1.0,4.0),
                ut.IntersectionPoint(points[5],points[0],s[0],s[1]),
                Point(2.0,3.7),
                Point(4.1,5.9)
    };
    vector<vector<int>> polygonPoints{
        { 0, 1, 2, 3, 11, 5, 6, 7, 10, 9 }, { 4, 5, 11, 3 }, { 8, 9, 10, 7 }
    };

    EXPECT_EQ(cut.GetNewPoints(), pointsNew);
    EXPECT_EQ(cut.GetCutPolygonsIndices(), polygonPoints);
}

TEST(TestPolygonCut, TestCut41)
{
    vector<Point> points{
        Point(2.0,-1.0),
                Point(7.0,1.0),
                Point(7.0,2.0),
                Point(5.0,3.7),
                Point(2.0,5.0),
                Point(1.0,3.0)
    };
    vector<int> vertices{0,1,2,3,4,5};
    vector<Point> s{
        Point(2.0,3.4),
                Point(5.6,1.3)
    };
    Utilities ut;
    PolygonCut cut(ut);
    cut.CutPolygon(points, vertices, s);
    vector<Point> pointsNew{
        Point(2.0,-1.0),
                ut.IntersectionPoint(points[0],points[1],s[0],s[1]),
                Point(7.0,1.0),
                Point(7.0,2.0),
                Point(5.0,3.7),
                Point(2.0,5.0),
                ut.IntersectionPoint(points[4],points[5],s[0],s[1]),
                Point(1.0,3.0),
                Point(2.0,3.4),
                Point(5.6,1.3)
    };
    vector<vector<int>> polygonPoints{
        { 0, 1, 8, 9, 6, 7 }, { 2, 3, 4, 5, 6, 8, 9, 1 }
    };

    EXPECT_EQ(cut.GetNewPoints(), pointsNew);
    EXPECT_EQ(cut.GetCutPolygonsIndices(), polygonPoints);
}

TEST(TestPolygonCut, TestCut51)
{
    vector<Point> points{
        Point(3.0,1.0),
                Point(4.0,-1.0),
                Point(5.0,0.5),
                Point(7.0,-0.3),
                Point(6.0,4.0),
                Point(1.0,3.0)
    };
    vector<int> vertices{0,1,2,3,4,5};
    vector<Point> s{
        Point(4.0,-0.2),
                Point(6.5,0.5)
    };
    Utilities ut;
    PolygonCut cut(ut);
    cut.CutPolygon(points, vertices, s);
    vector<Point> pointsNew{
        Point(3.0,1.0),
                ut.IntersectionPoint(points[0],points[1],s[0],s[1]),
                Point(4.0,-1.0),
                ut.IntersectionPoint(points[1],points[2],s[0],s[1]),
                Point(5.0,0.5),
                ut.IntersectionPoint(points[2],points[3],s[0],s[1]),
                Point(7.0,-0.3),
                ut.IntersectionPoint(points[3],points[4],s[0],s[1]),
                Point(6.0,4.0),
                Point(1.0,3.0),
                Point(4.0,-0.2),
                Point(6.5,0.5)
    };
    vector<vector<int>> polygonPoints{
        { 0, 1, 10, 3, 4, 5, 11, 7, 8, 9 }, { 2, 3, 10, 1 }, { 6, 7, 11, 5 }
    };

    EXPECT_EQ(cut.GetNewPoints(), pointsNew);
    EXPECT_EQ(cut.GetCutPolygonsIndices(), polygonPoints);
}

TEST(TestPolygonCut, TestCut61)
{
    vector<Point> points{
        Point(2.0, 4.0),
                Point(2.0, -3.0),
                Point(3.5, 1.0),
                Point(5.0, -2.0),
                Point(7.0, 3.0),
                Point(5.0, -1.0)
    };
    vector<int> vertices{0,1,2,3,4,5};
    vector<Point> s{
        Point(2.5,-0.5),
                Point(6.5,1.0)
    };
    Utilities ut;
    PolygonCut cut(ut);
    cut.CutPolygon(points, vertices, s);
    vector<Point> pointsNew{
        Point(2.0, 4.0),
                ut.IntersectionPoint(points[0],points[1],s[0],s[1]),
                Point(2.0, -3.0),
                ut.IntersectionPoint(points[1],points[2],s[0],s[1]),
                Point(3.5, 1.0),
                ut.IntersectionPoint(points[2],points[3],s[0],s[1]),
                Point(5.0, -2.0),
                ut.IntersectionPoint(points[3],points[4],s[0],s[1]),
                Point(7.0, 3.0),
                ut.IntersectionPoint(points[4],points[5],s[0],s[1]),
                Point(5.0, -1.0),
                ut.IntersectionPoint(points[5],points[0],s[0],s[1]),
                Point(2.5,-0.5)
    };
    vector<vector<int>> polygonPoints{
        { 0, 1, 12, 3, 4, 5, 11 }, { 2, 3, 12, 1 }, { 6, 7, 9, 10, 11, 5 }, { 8, 9, 7 }
    };

    EXPECT_EQ(cut.GetNewPoints(), pointsNew);
    EXPECT_EQ(cut.GetCutPolygonsIndices(), polygonPoints);
}
}
#endif // TESTCUTTING_H
