#include "PolygonCutting.hpp"

#include <iostream>
#include <vector>
#include <math.h>

namespace Cut{


Point::Point() {
    X = 0.0;
    Y = 0.0;
    isInt = false;
}

Point::Point(const double &x, const double &y) {
    X = x;
    Y = y;
    isInt = false;
}

Point::Point(const Point &point) {
    *this = point;
}

double Point::ComputeNorm2() const {
    return(sqrt(X*X+Y*Y));
}

Point Point::operator+(const Point& point) const
{
    return Point(X+point.X,Y+point.Y);
}

Point Point::operator-(const Point& point) const
{
    return Point(X-point.X,Y-point.Y);
}

Point&Point::operator-=(const Point& point)
{
    X = X-point.X;
    Y = Y-point.Y;
    return *this;
}

Point&Point::operator+=(const Point& point)
{
    X = X+point.X;
    Y = Y+point.Y;
    return *this;
}

bool Point::operator==(const Point &point) const
{
    if(float(point.X) == float(X) && float(point.Y) == float(Y))
        return true;
    return false;
}

bool Point::operator!=(const Point &point) const
{
    if(float(point.X) != float(X) || float(point.Y) != float(Y))
        return true;
    return false;
}

Polygon::Polygon()
{
    vertices.resize(1);
    vertices[0] = Point();
}

Polygon::Polygon(const vector<Point> &points)
{
    vertices.resize(points.size());
    for(unsigned int i=0; i<points.size(); i++)
        vertices[i] = points[i];
}

void Polygon::AddVertex(const Point &vertex)
{
    vertices.resize(vertices.size()+1);
    vertices.push_back(vertex);
}

bool Polygon::operator ==(Polygon &polygon)
{
    if(polygon.vertices.size() != vertices.size())
        return false;
    for(unsigned int i=0; i<vertices.size(); i++)
    {
        if(!(polygon.vertices[i] == vertices[i]))
            return false;
    }

    return true;
}

double Utilities::CalcDistance(const Point &p1, Point &p2)
{
    return sqrt((p1.X-p2.X)*(p1.X-p2.X)+(p1.Y-p2.Y)*(p1.Y-p2.Y));
}

vector<Point> Utilities::SortPoints(vector<Point> &points)
{
    vector<Point> orderedPoints;
    vector<Point>::iterator positionMin;
    double min;
    vector<Point>::iterator it = points.begin();
    Point point = *it;
    orderedPoints.push_back(* it);
    points.erase(it);
    unsigned int size = points.size();
    for(unsigned int i=0; i<size-1; i++)
    {
        min = CalcDistance(point,*points.begin());
        positionMin = it;
        for(vector<Point>::iterator jt = points.begin(); jt != points.end(); jt++)
        {
            if(CalcDistance(point, *jt)<min)
            {
                min = CalcDistance(point,*jt);
                positionMin = jt;
            }
        }
        orderedPoints.push_back(*positionMin);
        point = *positionMin;
        points.erase(positionMin);
    }
    orderedPoints.push_back(points[points.size()-1]);
    return orderedPoints;
}

int Utilities::FindPoint(const vector<Point> &pointsVector, const Point &pointToFind)
{
    for(unsigned int i=0; i<pointsVector.size(); i++)
    {
        if(pointsVector[i] == pointToFind)
        {
            return i;
        }
    }
    return -1;
}

bool Utilities::IsBetween(const Point &l1, const Point &l2, const Point &p)
{
    if((p.X-l1.X)/(l2.X-l1.X) - (p.Y-l1.Y)/(l2.Y-l1.Y) > tolerance || (p.X-l1.X)/(l2.X-l1.X) - (p.Y-l1.Y)/(l2.Y-l1.Y) < -tolerance)
        return false;
    double minX = l1.X;
    double maxX = l2.X;
    double minY = l1.Y;
    double maxY = l2.Y;
    if(l2.X < l1.X)
    {
        minX = l2.X;
        maxX = l1.X;
    }
    if(l2.Y < l1.Y)
    {
        minY = l2.Y;
        maxY = l1.Y;
    }
    if((minX <= p.X && p.X <= maxX) && (minY <= p.Y && p.Y <= maxY))
        return true;
    return false;
}

Type Utilities::CheckIntersection(const Point &s1, const Point &s2, const Point &one, const Point &two)
{
    Type type = NoIntersection;
    double det = (one.X-two.X)*(s1.Y-s2.Y)
            -(one.Y-two.Y)*(s1.X-s2.X);

    if (det > tolerance || det < -tolerance)
    {
        double t = ((one.X-s1.X)*(s1.Y-s2.Y)
                    -(one.Y-s1.Y)*(s1.X-s2.X))/det;
        if(t >= tolerance && t <= 1+tolerance)
            type = IntersectionOnSegment;
        else
            type = IntersectionOnLine;
    }
    return type;
}

Point Utilities::IntersectionPoint(const Point &s1, const Point &s2, const Point &one, const Point &two)
{
    Point point;

    if (CheckIntersection(s1,s2,one,two) != NoIntersection)
    {
        double t = ((one.X-s1.X)*(s1.Y-s2.Y)
                    -(one.Y-s1.Y)*(s1.X-s2.X))/((one.X-two.X)*(s1.Y-s2.Y)
                                                -(one.Y-two.Y)*(s1.X-s2.X));
        point.X = one.X+t*(two.X-one.X);
        point.Y = one.Y+t*(two.Y-one.Y);
    }
    else
    {
        throw runtime_error("I due segmenti non si intersecano");
    }
    return point;
}

void Utilities::PrintResults(const vector<Point> &newPoints, const vector<Polygon> &cutPolygons)
{
    cout << "newPoints = {";
    for(unsigned int i=0; i<newPoints.size()-1; i++)
        cout << newPoints[i] << ",";
    cout << newPoints[newPoints.size()-1] << "}" << endl;


    vector<int> formingPoints;
    vector<vector<int>> newPolygonsPoints;
    for(unsigned int i=0; i<cutPolygons.size(); i++)
    {
        for(unsigned int j=0; j<cutPolygons[i].vertices.size(); j++)
        {
            formingPoints.push_back(FindPoint(newPoints,cutPolygons[i].vertices[j]));
        }
        newPolygonsPoints.push_back(formingPoints);
        formingPoints.clear();
    }
    cout << "cutPolygons = {";
    for(unsigned int i=0; i<newPolygonsPoints.size()-1; i++)
    {
        cout << "{";
        for(unsigned int j=0; j<newPolygonsPoints[i].size()-1; j++)
            cout << newPolygonsPoints[i][j] << ", ";
        cout << newPolygonsPoints[i][newPolygonsPoints[i].size()-1] << "}, " << endl;
    }
    cout << "{";
    for(unsigned int j=0; j<newPolygonsPoints[newPolygonsPoints.size()-1].size()-1; j++)
        cout << newPolygonsPoints[newPolygonsPoints.size()-1][j] << ", ";
    cout << newPolygonsPoints[newPolygonsPoints.size()-1][newPolygonsPoints[newPolygonsPoints.size()-1].size()-1] << "} } " << endl;

}

void Utilities::PrintResultsMatlab(const vector<Point> &newPoints, const vector<Polygon> &cutPolygons)
{
    cout << "newPoints = [";
    for(unsigned int i=0; i<newPoints.size()-1; i++)
        cout << newPoints[i].X << "," << newPoints[i].Y << ";" << endl;
    cout << newPoints[newPoints.size()-1].X << "," << newPoints[newPoints.size()-1].Y << "];" << endl;


    cout << "cutPolygons = {";
    for(unsigned int i=0; i<cutPolygons.size()-1; i++)
    {
        cout << "[";
        for(unsigned int j=0; j<cutPolygons[i].vertices.size()-1; j++)
            cout << cutPolygons[i].vertices[j].X << "," << cutPolygons[i].vertices[j].Y << ";" << endl;
        cout << cutPolygons[i].vertices[cutPolygons[i].vertices.size()-1] << "], " << endl;
    }
    cout << "[";
    for(unsigned int j=0; j<cutPolygons[cutPolygons.size()-1].vertices.size()-1; j++)
        cout << cutPolygons[cutPolygons.size()-1].vertices[j].X << "," << cutPolygons[cutPolygons.size()-1].vertices[j].Y << ";" << endl;
    cout << cutPolygons[cutPolygons.size()-1].vertices[cutPolygons[cutPolygons.size()-1].vertices.size()-1].X << "," <<
                                                                                                                 cutPolygons[cutPolygons.size()-1].vertices[cutPolygons[cutPolygons.size()-1].vertices.size()-1].Y << "]; } " << endl;

}

void PolygonCut::CutPolygon(const vector<Point> &p, const vector<int> &vertices, const vector<Point> &s)
{
    // Creo un poligono con i punti e i vertici dati in input
    vector<Point> points(vertices.size());
    for(unsigned int i=0; i<vertices.size(); i++)
        points[i] = p[vertices[i]];
    Polygon polygon(points);

    // Inserisco in newPoints i vertici del poligono e le eventuali intersezioni tra le coppie,
    // nell'ordine in cui vengono trovate scorrendo i vertici
    vector<Point> intersectionPoints;
    newPoints.push_back(polygon.vertices[0]);
    for(unsigned int i=0; i<polygon.vertices.size(); i++)
    {
        if(computation.CheckIntersection(s[0],s[1],polygon.vertices[i],polygon.vertices[(i+1)%polygon.vertices.size()]) == IntersectionOnSegment)
        {
            Point newP = computation.IntersectionPoint(s[0],s[1],polygon.vertices[i],polygon.vertices[(i+1)%polygon.vertices.size()]);
            newP.isInt = true; // Serve per segnalare successivamente che il punto è un'intersezione
            newPoints.push_back(newP);
            intersectionPoints.push_back(newP);
            if(!(polygon.vertices[i+1] == newP) && (i+1 != polygon.vertices.size())) // Se l'intersezione è uno dei vertici
                newPoints.push_back(polygon.vertices[i+1]);                          // la inserisco solo una volta
        }
        else if (i+1 != polygon.vertices.size())
            newPoints.push_back(polygon.vertices[i+1]);
    }

    if(intersectionPoints.size() > 2)
        intersectionPoints = computation.SortPoints(intersectionPoints); // Le intersezioni vengono vengono ordinate per
    // accoppiare ogni punto "uscente" con il relativo
    // punto entrante
    vector<Point> formingPolygon;
    vector<bool> preso(newPoints.size()); // Indica se il punto è già presente in uno dei poligoni tagliati
    for(unsigned int i=0; i<newPoints.size(); i++)
        preso[i] = false;

    int pos;
    int conto = 0;
    // Scorro newPoints per formare i poligoni tagliati; partendo da un punto non di intersezione e non ancora "trovato",
    // prendo i successivi fino a trovare un'intersezione: a quel punto, mi sposto sul segmento fino all'intersezione successiva
    // e da lì riprendo a scorrere newPoints. Il ciclo while continua finché non si chiude un poligono, ovvero fino a quando
    // non mi ritrovo al punto di partenza
    for(unsigned int i=0; i < newPoints.size(); i++)
    {
        if(!preso[i] && !newPoints[i].isInt)
        {
            formingPolygon.push_back(newPoints[i]);
            preso[i] = true;
            unsigned int now = (i+1)%newPoints.size();
            while(now != i)
            {
                if(newPoints[now].isInt == true)
                {
                    preso[now] = true;
                    formingPolygon.push_back(newPoints[now]);
                    pos = computation.FindPoint(intersectionPoints, newPoints[now]);
                    if(pos == conto)
                    {
                        now = computation.FindPoint(newPoints,intersectionPoints[conto+1]);
                        if(computation.IsBetween(intersectionPoints[pos],intersectionPoints[pos+1],s[0])
                                && !(s[0] == intersectionPoints[pos]) && !(s[0] == intersectionPoints[pos+1]))
                            formingPolygon.push_back(s[0]);
                        if(computation.IsBetween(intersectionPoints[pos],intersectionPoints[pos+1],s[1])
                                && !(s[1] == intersectionPoints[pos]) && !(s[1] == intersectionPoints[pos+1]))
                            formingPolygon.push_back(s[1]);
                        conto = conto +2;
                    }
                    else
                    {
                        now = computation.FindPoint(newPoints, intersectionPoints[pos-1]);
                        if(computation.IsBetween(intersectionPoints[pos],intersectionPoints[pos-1],s[0])
                                && !(s[0] == intersectionPoints[pos]) && !(s[0] == intersectionPoints[pos-1]))
                            formingPolygon.push_back(s[0]);
                        if(computation.IsBetween(intersectionPoints[pos],intersectionPoints[pos-1],s[1])
                                && !(s[1] == intersectionPoints[pos]) && !(s[1] == intersectionPoints[pos-1]))
                            formingPolygon.push_back(s[1]);
                    }
                    preso[now]= true;
                    formingPolygon.push_back(newPoints[now]);
                }
                else
                {
                    preso[now] = true;
                    formingPolygon.push_back(newPoints[now]);
                }
                now = (now+1)%newPoints.size();
            }
            Polygon poly(formingPolygon);
            cutPolygons.push_back(poly);
            formingPolygon.clear();
        }
    }
    // Se gli estremi del segmento non sono ancora in newPoints, li inserisco adesso.
    if(computation.FindPoint(newPoints,s[0]) == -1
            && computation.IsBetween(intersectionPoints[0],intersectionPoints[intersectionPoints.size()-1],s[0]))
    {
        Point newP = s[0];
        newP.isInt = true;
        newPoints.push_back(newP);
    }
    if(computation.FindPoint(newPoints,s[1]) == -1
            && computation.IsBetween(intersectionPoints[0],intersectionPoints[intersectionPoints.size()-1],s[1]))
    {
        Point newP = s[1];
        newP.isInt = true;
        newPoints.push_back(newP);
    }

}

const vector<vector<int> > PolygonCut::GetCutPolygonsIndices()
{
    vector<vector<int>> newPolygonsPoints;
    vector<int> formingPoints;
    for(unsigned int i=0; i<cutPolygons.size(); i++)
    {
        for(unsigned int j=0; j<cutPolygons[i].vertices.size(); j++)
        {
            formingPoints.push_back(computation.FindPoint(newPoints,cutPolygons[i].vertices[j]));
        }
        newPolygonsPoints.push_back(formingPoints);
        formingPoints.clear();
    }
    return newPolygonsPoints;
}



}
