#include "shape.h"
#include <math.h>

namespace ShapeLibrary {

Ellipse::Ellipse() {
    _center = Point();
    _a = 0.0;
    _b = 0.0;
}

Ellipse::Ellipse(const Point &center, const double &a, const double &b) {
    _center = center;
    _a = a;
    _b = b;
}

double Ellipse::Perimeter() const
{
    return 2*M_PI*sqrt((_a*_a+_b*_b)/2);
}

Triangle::Triangle() {
    points.resize(3);
    points[0]=Point();
    points[1]=Point();
    points[2]=Point();
}

Triangle::Triangle(const Point& p1,
                   const Point& p2,
                   const Point& p3)
{
    points.resize(3);
    points[0]=p1;
    points[1]=p2;
    points[2]=p3;

}

void Triangle::AddVertex(const Point &point) {
    points.resize(points.size()+1);
    points[points.size()-1]=point;
}

double Triangle::Perimeter() const
{
    double perimeter = 0;
    unsigned int size = points.size();
    unsigned int j = 0;
    for(unsigned int i = 0; i<size; i++)
    {
        j = (i+1)%size;
        perimeter += sqrt((points[j].X-points[i].X)*(points[j].X-points[i].X)+(points[j].Y-points[i].Y)*(points[j].Y-points[i].Y));
}

    return perimeter;
}

TriangleEquilateral::TriangleEquilateral(const Point& p1,
                                         const double& edge)
{
    double height = sqrt(edge*edge-(edge*edge)/4);
    points[0]=p1;
    points[1]=Point(p1.X+edge/2,p1.Y-height);
    points[2]=Point(p1.X-edge/2,p1.Y-height);
}



Quadrilateral::Quadrilateral() {
    points.resize(4);
    points[0]=Point();
    points[1]=Point();
    points[2]=Point();
    points[3]=Point();
}

Quadrilateral::Quadrilateral(const Point& p1,
                             const Point& p2,
                             const Point& p3,
                             const Point& p4)
{
    points.resize(4);
    points[0]=p1;
    points[1]=p2;
    points[2]=p3;
    points[3]=p4;
}

void Quadrilateral::AddVertex(const Point &p) {
    points.resize(points.size()+1);
    points[points.size()-1]=p;
}

double Quadrilateral::Perimeter() const
{
    double perimeter = 0;
    unsigned int size = points.size();
    unsigned int j = 0;
    for(unsigned int i = 0; i<size; i++)
    {
        j = (i+1)%size;
        perimeter += sqrt((points[j].X-points[i].X)*(points[j].X-points[i].X)+(points[j].Y-points[i].Y)*(points[j].Y-points[i].Y));
}

    return perimeter;
}

Rectangle::Rectangle() {
    points.resize(4);
    points[0]=Point();
    points[1]=Point();
    points[2]=Point();
    points[3]=Point();
}

Rectangle::Rectangle(const Point &p1, const Point &p2, const Point &p3, const Point &p4) {
    points.resize(4);
    points[0]=p1;
    points[1]=p2;
    points[2]=p3;
    points[3]=p4;
}

Rectangle::Rectangle(const Point& p1,
                     const double& base,
                     const double& height)
{
    points.resize(4);
    points[0]=p1;
    points[1]=Point(p1.X,p1.Y+height);
    points[2]=Point(p1.X+base,p1.Y+height);
    points[3]=Point(p1.X+base,p1.Y);
}

Point::Point() {
    X = 0.0;
    Y = 0.0;
}

Point::Point(const double &x, const double &y) {
    X = x;
    Y = y;
}

Point::Point(const Point &point) {
    *this = point;
}

double Point::ComputeNorm2() const {
    return(sqrt(X*X+Y*Y));
}

Point Point::operator+(const Point& point) const
{
    return Point(X+point.X,Y+point.Y);
}

Point Point::operator-(const Point& point) const
{
    return Point(X-point.X,Y-point.Y);
}

Point&Point::operator-=(const Point& point)
{
    X = X-point.X;
    Y = Y-point.Y;
    return *this;
}

Point&Point::operator+=(const Point& point)
{
    X = X+point.X;
    Y = Y+point.Y;
    return *this;
}

Circle::Circle() {
    _center = Point();
    _a = 0.0;
    _b = 0.0;
}

Circle::Circle(const Point &center, const double &radius) {
    _center = center;
    _a = radius;
    _b = radius;
}

Square::Square() {
    points.resize(4);
    points[0]=Point();
    points[1]=Point();
    points[2]=Point();
    points[3]=Point();
}

Square::Square(const Point &p1, const Point &p2, const Point &p3, const Point &p4) {
    points.resize(4);
    points[0]=p1;
    points[1]=p2;
    points[2]=p3;
    points[3]=p4;
}

Square::Square(const Point &p1, const double &edge) {
    points[0]=p1;
    points[1]=Point(p1.X,p1.Y+edge);
    points[2]=Point(p1.X+edge,p1.Y+edge);
    points[3]=Point(p1.X+edge,p1.Y);
}



}
