#include "Pizzeria.h"

namespace PizzeriaLibrary {

Ingredient::Ingredient(const string &name, const int &price, const string &description)
{
    Name = name;
    Price = price;
    Description = description;
}

Ingredient::Ingredient()
{
    Name = "";
    Price = 0;
    Description = "";
}

Pizza::Pizza(const string &name)
{
    Name = name;
}

Pizza::Pizza(const string &name, vector<Ingredient> &ingredients)
{
    Name = name;
    Ingredients = ingredients;
}

void Pizza::AddIngredient(const Ingredient &ingredient)
{
    Ingredients.push_back(ingredient);
}

int Pizza::NumIngredients() const
{
    return Ingredients.size();
}

int Pizza::ComputePrice() const
{
    unsigned int price = 0;
    unsigned int numElements = NumIngredients();
    for(unsigned int i=0; i<numElements; i++)
        price = price+Ingredients[i].Price;
    return price;
}

Order::Order(vector<Pizza> pizzas)
{
    InitializeOrder(pizzas.size());
    Pizzas = pizzas;
}

void Order::InitializeOrder(int numPizzas)
{
    Pizzas.reserve(numPizzas);
}

void Order::AddPizza(const Pizza &pizza)
{
    Pizzas.push_back(pizza);
}

const Pizza &Order::GetPizza(const int &position) const
{
    if(position>=Pizzas.size())
        throw runtime_error("Position passed is wrong");
    return Pizzas[position];
}

int Order::NumPizzas() const
{
    return Pizzas.size();
}

int Order::ComputeTotal() const
{
    unsigned int price = 0;
    for(unsigned int i=0; i<NumPizzas(); i++)
        price = price+Pizzas[i].ComputePrice();
    return price;
}

void Pizzeria::AddIngredient(const string &name, const string &description, const int &price)
{
    Ingredient newIngredient(name,price,description);
    for(unsigned int i=0; i<Ingredients.size(); i++)
    {
        if(Ingredients[i].Name==name)
            throw runtime_error("Ingredient already inserted");
    }
    Ingredients.push_back(newIngredient);
}

const Ingredient &Pizzeria::FindIngredient(const string &name) const
{
    for(unsigned int i=0; i<Ingredients.size(); i++)
    {
        if(Ingredients[i].Name==name)
        {
            return Ingredients[i];
        }
    }
    throw runtime_error("Ingredient not found");
}

void Pizzeria::AddPizza(const string &name, const vector<string> &ingredients)
{
    for(unsigned int i=0; i<Pizzas.size(); i++)
    {
        if(Pizzas[i].Name==name)
            throw runtime_error("Pizza already inserted");
    }
    vector<Ingredient> ingredientsVector;
    ingredientsVector.reserve(ingredients.size());
    Ingredient ingr;
    for(unsigned int i=0; i<ingredients.size(); i++)
    {
        ingr = FindIngredient(ingredients[i]);
        string name = ingr.Name;
        unsigned int price = ingr.Price;
        string description = ingr.Description;
        Ingredient newIngredient(name,price,description);
        ingredientsVector.push_back(newIngredient);
    }
    Pizza newPizza(name,ingredientsVector);
    Pizzas.push_back(newPizza);
}

const Pizza &Pizzeria::FindPizza(const string &name) const
{
    unsigned int numPizzas = Pizzas.size();
    for(unsigned int i=0; i<numPizzas; i++)
    {
        if(Pizzas[i].Name==name)
        {
            return Pizzas[i];
            break;
        }
    }
    throw runtime_error("Pizza not found");
}

int Pizzeria::CreateOrder(const vector<string> &pizzas)
{
    vector<Pizza> pizzasVector;
    if(pizzas.size()==0)
        throw runtime_error("Empty order");
    for(unsigned int i=0; i<pizzas.size(); i++)
    {
        Pizza newPizza = FindPizza(pizzas[i]);
        pizzasVector.push_back(newPizza);
    }
    Order newOrder(pizzasVector);
    Orders.push_back(newOrder);
    return Orders.size()+999;
}

const Order &Pizzeria::FindOrder(const int &numOrder) const
{
    if(numOrder-999>Orders.size())
        throw runtime_error("Order not found");
    return(Orders[numOrder-1000]);
}

string Pizzeria::GetReceipt(const int &numOrder) const
{
    string receipt = "";
    int total = 0;
    Order newOrder = FindOrder(numOrder);
    for(unsigned int j=0; j<newOrder.Pizzas.size(); j++)
    {
        receipt = receipt + "- " + newOrder.Pizzas[j].Name + ", " + to_string(newOrder.Pizzas[j].ComputePrice()) + " euro" + "\n";
        total = total + newOrder.Pizzas[j].ComputePrice();
    }
    receipt = receipt + "  TOTAL: " + to_string(total) + " euro" + "\n";
    return receipt;
}

string Pizzeria::ListIngredients() const
{
    string list = "";
    set<string> listIngredients;
    for(unsigned int i=0; i<Ingredients.size(); i++)
        listIngredients.insert(Ingredients[i].Name);
    for(set<string>::iterator it = listIngredients.begin(); it != listIngredients.end(); it++)
        list = list + FindIngredient(*it).Name + " - '" + FindIngredient(*it).Description + "': " + to_string(FindIngredient(*it).Price) + " euro" + "\n";
    return list;
}

string Pizzeria::Menu() const
{
    string list = "";
    set<string> listPizzas;
    for(unsigned int i=0; i<Pizzas.size(); i++)
        listPizzas.insert(Pizzas[i].Name);
    for(set<string>::iterator it = listPizzas.begin(); it != listPizzas.end(); it++)
        list = list + FindPizza(*it).Name + " (" + to_string(FindPizza(*it).Ingredients.size()) + " ingredients): " + to_string(FindPizza(*it).ComputePrice()) + " euro" + "\n";
    return list;
}

}
