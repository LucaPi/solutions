#ifndef PIZZERIA_H
#define PIZZERIA_H

#include <iostream>
#include <vector>
#include <set>

using namespace std;

namespace PizzeriaLibrary {

class Ingredient {
public:
    string Name;
    int Price;
    string Description;
    Ingredient(const string& name,
               const int& price,
               const string& description);
    Ingredient();
};

class Pizza {
public:
    string Name;
    vector<Ingredient> Ingredients;
    Pizza(const string& name);
    Pizza(const string& name,
          vector<Ingredient>& ingredients);
    void AddIngredient(const Ingredient& ingredient);
    int NumIngredients() const;
    int ComputePrice() const;
};

class Order {
public:
    vector<Pizza> Pizzas;
    Order(vector<Pizza> pizzas);
    void InitializeOrder(int numPizzas);
    void AddPizza(const Pizza& pizza);
    const Pizza& GetPizza(const int& position) const;
    int NumPizzas() const;
    int ComputeTotal() const;
};


class Pizzeria {
public:
    vector<Ingredient> Ingredients;
    vector<Pizza> Pizzas;
    vector<Order> Orders;
    void AddIngredient(const string& name,
                       const string& description,
                       const int& price);
    const Ingredient& FindIngredient(const string& name) const;
    void AddPizza(const string& name,
                  const vector<string>& ingredients);
    const Pizza& FindPizza(const string& name) const;
    int CreateOrder(const vector<string>& pizzas);
    const Order& FindOrder(const int& numOrder) const;
    string GetReceipt(const int& numOrder) const;
    string ListIngredients() const;
    string Menu() const;
};
};

#endif // PIZZERIA_H
