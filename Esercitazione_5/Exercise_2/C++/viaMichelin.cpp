#include "viaMichelin.h"

#include <iostream>
#include <fstream>
#include <sstream>

namespace ViaMichelinLibrary {
int RoutePlanner::BusAverageSpeed = 50;

void BusStation::Reset()
{
    _numberBuses = 0;
    _buses.clear();
}

void BusStation::Load() {
    Reset();

    ifstream file;
    file.open(_busFilePath.c_str());

    if (file.fail())
        throw runtime_error("Something goes wrong");

    try {
        string line;
        getline(file, line); // Comment
        getline(file, line);
        istringstream convertNumber;
        convertNumber.str(line);
        convertNumber >> _numberBuses;

        getline(file,line); // Comment
        getline(file,line); // Comment

        _buses.resize(_numberBuses);
        for (int b = 0; b < _numberBuses; b++)
        {
            getline(file,line);
            istringstream convertBus;
            convertBus.str(line);
            convertBus >> _buses[b].Id >> _buses[b].FuelCost;
        }

        file.close();
    }
    catch (exception) {
        Reset();
        throw runtime_error("Something goes wrong");
    }
}

int BusStation::NumberBuses() const  { return _numberBuses; }

const Bus &BusStation::GetBus(const int &idBus) const {
    if(idBus > _numberBuses)
        throw runtime_error("Bus " + to_string(idBus) + " does not exists");

    return _buses[idBus - 1];
}

void MapData::Reset()
{
    _numberRoutes = 0;
    _numberStreets = 0;
    _numberStops = 0;
    _routes.clear();
    _streets.clear();
    _busStops.clear();
    _streetFrom.clear();
    _streetTo.clear();
    _routeStreets.clear();
}

void MapData::Load() {
    Reset();

    ifstream file;
    file.open(_mapFilePath.c_str());

    if(file.fail())
        throw runtime_error("Something goes wrong");

    try{
        string line;

        getline(file,line); // Comment
        getline(file,line);
        istringstream convertB;
        convertB.str(line);
        convertB >> _numberStops;

        getline(file,line); // Comment
        _busStops.resize(_numberStops);
        for(int s=0; s<_numberStops; s++)
        {
            getline(file,line);
            istringstream convertStop;
            convertStop.str(line);
            convertStop >> _busStops[s].Id >> _busStops[s].Name >> _busStops[s].Latitude >> _busStops[s].Longitude;
        }

        getline(file,line); // Comment
        getline(file,line);
        istringstream convertS;
        convertS.str(line);
        convertS >> _numberStreets;

        getline(file,line); // Comment
        _streets.resize(_numberStreets);
        _streetFrom.resize(_numberStreets);
        _streetTo.resize(_numberStreets);
        for(int s=0; s<_numberStreets; s++)
        {
            getline(file,line);
            istringstream convertStreet;
            convertStreet.str(line);
            convertStreet >> _streets[s].Id  >> _streetFrom[s] >> _streetTo[s] >> _streets[s].TravelTime;
        }

        getline(file,line); // Comment
        getline(file,line);
        istringstream convertR;
        convertR.str(line);
        convertR >> _numberRoutes;

        getline(file,line); // Comment
        _routes.resize(_numberRoutes);
        _routeStreets.resize(_numberRoutes);
        for(int s=0; s<_numberRoutes; s++)
        {
            getline(file,line);
            istringstream convertRoute;
            convertRoute.str(line);
            convertRoute >> _routes[s].Id  >> _routes[s].NumberStreets;
            _routeStreets[s].resize(_routes[s].NumberStreets);

            for (int r = 0; r < _routes[s].NumberStreets; r++)
                convertRoute >> _routeStreets[s][r];
        }

        file.close();
    }
    catch (exception) {
        Reset();
        throw runtime_error("Something goes wrong");
    }
}

int MapData::NumberRoutes() const { return _numberRoutes; }

int MapData::NumberStreets() const { return _numberStreets; }

int MapData::NumberBusStops() const { return _numberStops; }

const Street &MapData::GetRouteStreet(const int &idRoute, const int &streetPosition) const {
    if(idRoute>_numberRoutes)
        throw runtime_error("Route " + to_string(idRoute) + " does not exists");

    const Route& route = _routes[idRoute - 1];

    if (streetPosition >= route.NumberStreets)
        throw runtime_error("Street at position " + to_string(streetPosition) + " does not exists");

    int idStreet = _routeStreets[idRoute - 1][streetPosition];

    return _streets[idStreet - 1];
}

const Route &MapData::GetRoute(const int &idRoute) const {
    if(idRoute>_numberRoutes)
        throw runtime_error("Route " + to_string(idRoute) + " does not exists");

    return _routes[idRoute - 1];
}

const Street &MapData::GetStreet(const int &idStreet) const {
    if(idStreet>_numberStreets)
        throw runtime_error("Street " + to_string(idStreet) + " does not exists");

    return _streets[idStreet - 1];
}

const BusStop &MapData::GetStreetFrom(const int &idStreet) const {
    if(idStreet>_numberStreets)
        throw runtime_error("Street " + to_string(idStreet) + " does not exists");

    int from = _streetFrom[idStreet - 1];

    return _busStops[from - 1];
}

const BusStop &MapData::GetStreetTo(const int &idStreet) const {
    if(idStreet>_numberStreets)
        throw runtime_error("Street " + to_string(idStreet) + " does not exists");

    int to = _streetTo[idStreet - 1];

    return _busStops[to - 1];
}

const BusStop &MapData::GetBusStop(const int &idBusStop) const {
    if(idBusStop > _numberStops)
        throw runtime_error("BusStop " + to_string(idBusStop) + " does not exists");

    return _busStops[idBusStop - 1];
}

int RoutePlanner::ComputeRouteTravelTime(const int &idRoute) const {
    const Route& route = _mapData.GetRoute(idRoute);

    int travelTime = 0;
    for (int s = 0; s < route.NumberStreets; s++)
        travelTime += _mapData.GetRouteStreet(idRoute, s).TravelTime;

    return travelTime;
}

int RoutePlanner::ComputeRouteCost(const int &idBus, const int &idRoute) const {
    const Route& route = _mapData.GetRoute(idRoute);

    int totalCost = 0;
    for (int s = 0; s < route.NumberStreets; s++)
        totalCost += _mapData.GetRouteStreet(idRoute, s).TravelTime * _busStation.GetBus(idBus).FuelCost * BusAverageSpeed;

    return totalCost/3600;

}

string MapViewer::ViewRoute(const int &idRoute) const {
    //Route: Id": + " foreach street + From.Name + "->" + last street + To.Name
    const Route& route = _mapData.GetRoute(idRoute);

    int s=0;
    ostringstream routeView;
    routeView<< to_string(idRoute)<< ": ";
    for (; s < route.NumberStreets - 1; s++)
    {
        int idStreet = _mapData.GetRouteStreet(idRoute, s).Id;
        string from = _mapData.GetStreetFrom(idStreet).Name;
        routeView << from<< " -> ";
    }

    int idStreet = _mapData.GetRouteStreet(idRoute, s).Id;
    string from = _mapData.GetStreetFrom(idStreet).Name;
    string to = _mapData.GetStreetTo(idStreet).Name;
    routeView << from<< " -> "<< to;

    return routeView.str();
}

string MapViewer::ViewStreet(const int &idStreet) const {
    const BusStop& from = _mapData.GetStreetFrom(idStreet);
    const BusStop& to = _mapData.GetStreetTo(idStreet);
    return to_string(idStreet) + ": " + from.Name + " -> " + to.Name;
}

string MapViewer::ViewBusStop(const int &idBusStop) const {
    const BusStop& busStop = _mapData.GetBusStop(idBusStop);
    return busStop.Name + " (" + to_string(busStop.Latitude/10000.0) + ", " + to_string(busStop.Longitude/10000.0) + ")";
}

}
